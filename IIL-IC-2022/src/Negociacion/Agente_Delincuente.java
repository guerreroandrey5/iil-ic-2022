/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negociacion;

import Datos.DBData;
import Objetos.objAGDL;
import Objetos.objAG_DL;
import java.util.ArrayList;

/**
 *
 * @author Guerrero
 */
public class Agente_Delincuente {
    DBData DB = new DBData();
    public void InsAGDL(objAG_DL AGDL){
        DB.insertarAG_DL(AGDL);
    }
    
    public ArrayList<objAGDL> loadDelinTbl(int codigo){
        ArrayList<objAGDL> DeLinList = DB.loadDeli(codigo);
        return DeLinList;       
    }
    
}
