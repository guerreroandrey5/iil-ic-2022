/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negociacion;

import Datos.DBData;
import Objetos.objAgente;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author Guerrero
 */
public class Agente {
    DBData DB = new DBData();
    public void InsAgent(objAgente Agent){
        DB.insertarAgente(Agent);
    }
public DefaultComboBoxModel loadAGNames(){
    DefaultComboBoxModel model = new DefaultComboBoxModel();
    ArrayList<String> lNames = DB.loadAGNames();
    for (String Names : lNames){
        model.addElement(Names);
    }
    return model;
    }
}
