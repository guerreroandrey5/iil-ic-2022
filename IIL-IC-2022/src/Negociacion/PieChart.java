/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negociacion;

import Objetos.objctTipDe;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.util.Rotation;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

/**
 *
 * @author Guerrero
 */
public class PieChart extends JFrame{
    TipoDelincuentes TD = new TipoDelincuentes();
    public PieChart(String appTitle, String chartTitle) throws FileNotFoundException{
        PieDataset dataset = createDataset();
        JFreeChart chart = createChart(dataset, chartTitle);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 300));
        setContentPane(chartPanel);           
    }


    
    private PieDataset createDataset() throws FileNotFoundException{  
        objctTipDe.listatipdei = new ArrayList<>();
        ArrayList<String> listDel = TD.loadCantTp();  
        int tam = listDel.size();
        System.out.println(tam);
        System.out.println(listDel);
        int lad = Collections.frequency(listDel, "Ladrón");
        int ase = Collections.frequency(listDel, "Asesino");
        int narc = Collections.frequency(listDel, "Narcotraficante");
        int secu = Collections.frequency(listDel, "Secuestrador");
        System.out.println(lad);
        System.out.println(ase);
        System.out.println(narc);
        System.out.println(secu);
        DefaultPieDataset result = new DefaultPieDataset();
        result.setValue("Asesinos",ase);
        result.setValue("Narcotraficantes",narc);
        result.setValue("Ladrón",lad);
        result.setValue("Secuestradores",secu);
        return result;
    }
            
    private JFreeChart createChart (PieDataset dataset, String title){
        JFreeChart chart = ChartFactory.createPieChart3D(title, dataset, true, true, false);
        PiePlot3D plot = (PiePlot3D) chart.getPlot();
        plot.setStartAngle(0);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
        return chart;
    }
    
}
