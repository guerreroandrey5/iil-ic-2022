/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Objetos;

/**
 *
 * @author Guerrero
 */
public class objAG_DL {
    private int codigo;
    private int codAgent;
    private int codDelin;

    public objAG_DL(int codigo, int codAgent, int codDelin) {
        this.codigo = codigo;
        this.codAgent = codAgent;
        this.codDelin = codDelin;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodAgent() {
        return codAgent;
    }

    public void setCodAgent(int codAgent) {
        this.codAgent = codAgent;
    }

    public int getCodDelin() {
        return codDelin;
    }

    public void setCodDelin(int codDelin) {
        this.codDelin = codDelin;
    }
    
}
