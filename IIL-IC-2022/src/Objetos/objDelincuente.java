/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Objetos;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Guerrero
 */
public class objDelincuente {

    private int codigo;
    private String nombre;
    private Date fechanaci;
    private String paisor;
    private int recom;
    private String alias;
    private int tipodeli;
    private Date fechaprdel;
    private int cantdel;
    private int orga;
   
    public static ArrayList listaDelin = new ArrayList<>();
    
    public objDelincuente(int codigo, String nombre, Date fechanaci, String paisor, int recom, String alias, int tipodeli, Date fechaprdel, int cantdel, int orga) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.fechanaci = fechanaci;
        this.paisor = paisor;
        this.recom = recom;
        this.alias = alias;
        this.tipodeli = tipodeli;
        this.fechaprdel = fechaprdel;
        this.cantdel = cantdel;
        this.orga = orga;
    }

    public objDelincuente() {
        
    }

    public int getOrga() {
        return orga;
    }

    public objDelincuente(int orga) {
        this.orga = orga;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechanaci() {
        return fechanaci;
    }

    public void setFechanaci(Date fechanaci) {
        this.fechanaci = fechanaci;
    }

    public String getPaisor() {
        return paisor;
    }

    public void setPaisor(String paisor) {
        this.paisor = paisor;
    }

    public int getRecom() {
        return recom;
    }

    public void setRecom(int recom) {
        this.recom = recom;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getTipodeli() {
        return tipodeli;
    }

    public void setTipodeli(int tipodeli) {
        this.tipodeli = tipodeli;
    }

    public Date getFechaprdel() {
        return fechaprdel;
    }

    public void setFechaprdel(Date fechaprdel) {
        this.fechaprdel = fechaprdel;
    }

    public int getCantdel() {
        return cantdel;
    }

    public void setCantdel(int cantdel) {
        this.cantdel = cantdel;
    }
    
}
