/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Objetos;

import java.util.ArrayList;

/**
 *
 * @author Guerrero
 */
public class objAGDL {
    private String ahnombre;
    private String dnombre;
    private String aliz;
    private String tpnombre;
    private String onombre;
    public static ArrayList listaDelin = new ArrayList<>();
    public objAGDL(String ahnombre, String dnombre, String aliz, String tpnombre, String onombre) {
        this.ahnombre = ahnombre;
        this.dnombre = dnombre;
        this.aliz = aliz;
        this.tpnombre = tpnombre;
        this.onombre = onombre;
    }

    public String getAhnombre() {
        return ahnombre;
    }

    public void setAhnombre(String ahnombre) {
        this.ahnombre = ahnombre;
    }

    public String getDnombre() {
        return dnombre;
    }

    public void setDnombre(String dnombre) {
        this.dnombre = dnombre;
    }

    public String getAliz() {
        return aliz;
    }

    public void setAliz(String aliz) {
        this.aliz = aliz;
    }

    public String getTpnombre() {
        return tpnombre;
    }

    public void setTpnombre(String tpnombre) {
        this.tpnombre = tpnombre;
    }

    public String getOnombre() {
        return onombre;
    }

    public void setOnombre(String onombre) {
        this.onombre = onombre;
    }
    
}
