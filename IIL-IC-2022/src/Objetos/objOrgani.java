/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Objetos;

import java.util.Date;

/**
 *
 * @author Guerrero
 */
public class objOrgani {
   private int codigo;
   private String nombre;
   private String tipo;
   private String descrip;
   private Date fechafund;

    public objOrgani(int codigo, String nombre, String tipo, String descrip, Date fechafund) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.tipo = tipo;
        this.descrip = descrip;
        this.fechafund = fechafund;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public Date getFechafund() {
        return fechafund;
    }

    public void setFechafund(Date fechafund) {
        this.fechafund = fechafund;
    }
   
}
