/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Datos;

import Objetos.objAGDL;
import Objetos.objAG_DL;
import Objetos.objAgente;
import Objetos.objDelincuente;
import Objetos.objOrgani;
import Objetos.objTipoDel;
import Objetos.objctTipDe;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 *
 * @author Guerrero
 */
public class DBData {
    private ResultSet rs = null;
    private Statement s = null;
    DBConx conx = new DBConx();
    private Connection connection = null;
    public void insertarDelin(objDelincuente Delin){
        try{
            connection = conx.Conxx();
            s = connection.createStatement();
            int z = s.executeUpdate("insert into delincuentes(codigo,nombre,fecha_naci,paisor,recom,organi,fecha_pr_del,cantdel,aliaa,tipodel) values("
                    + Delin.getCodigo() + ",'" + Delin.getNombre() + "','" + Delin.getFechanaci() + "','" + Delin.getPaisor() + "'," + Delin.getRecom() + "," + Delin.getOrga() 
                    + ",'" + Delin.getFechaprdel() + "'," + Delin.getCantdel() + ",'" + Delin.getAlias() + "'," + Delin.getTipodeli()+")");
            if (z==1){
                JOptionPane.showMessageDialog(null,"El Delincuente ha sido agregado exitosamente","Alerta", JOptionPane.INFORMATION_MESSAGE);
            }else{
                JOptionPane.showMessageDialog(null,"Error al agregar, verifique datos","Alerta", JOptionPane.INFORMATION_MESSAGE);
            }
            
        }catch (Exception e){
            System.out.println("Error en el Query SQL: " + e);
        }
    }
    
    public void insertarTipDeli(objTipoDel TipDel){
        try{
            connection = conx.Conxx();
            s = connection.createStatement();
            int z = s.executeUpdate("insert into tipo_deli(codigo,nombre) values(" + TipDel.getCodigo() + ",'" + TipDel.getNombre() +"')");           
            if (z==1){
                JOptionPane.showMessageDialog(null,"El Tipo de Delincuencia ha sido agregado exitosamente","Alerta", JOptionPane.INFORMATION_MESSAGE);
            }else{
                JOptionPane.showMessageDialog(null,"Error al agregar, verifique datos","Alerta", JOptionPane.INFORMATION_MESSAGE);
            }
        }catch (Exception e){
            System.out.println("Error en el Query SQL: " + e);
        }
    }
    
    public void insertarOrgani(objOrgani Organ){
        try{
            connection = conx.Conxx();
            s = connection.createStatement();
            int z = s.executeUpdate("insert into organi(codigo,nombre,tipo,descripcion,fecha_fund) values(" + Organ.getCodigo() + ",'" + Organ.getNombre() + "','" + Organ.getTipo() + "','" + Organ.getDescrip() + "','" + Organ.getFechafund() + "')");           
            if (z==1){
                JOptionPane.showMessageDialog(null,"La Organizació Criminal sido agregada exitosamente","Alerta", JOptionPane.INFORMATION_MESSAGE);
            }else{
                JOptionPane.showMessageDialog(null,"Error al agregar, verifique datos","Alerta", JOptionPane.INFORMATION_MESSAGE);
            }
        }catch (Exception e){
            System.out.println("Error en el Query SQL: " + e);
        }
    }
    public void insertarAgente(objAgente Agent){
        try{
            connection = conx.Conxx();
            s = connection.createStatement();
            int z = s.executeUpdate("insert into agent (codigo,nombre,direccion,telefono) values(" + Agent.getCodigo() + ",'" + Agent.getNombre() + "','" + Agent.getDirec() + "'," + Agent.getTelefono() + ")");           
            if (z==1){
                JOptionPane.showMessageDialog(null,"El Agente ha sido agregado exitosamente","Alerta", JOptionPane.INFORMATION_MESSAGE);
            }else{
                JOptionPane.showMessageDialog(null,"Error al agregar, verifique datos","Alerta", JOptionPane.INFORMATION_MESSAGE);
            }
        }catch (Exception e){
            System.out.println("Error en el Query SQL: " + e);
        }
    }
    public void insertarAG_DL(objAG_DL AGDL){
        try{
            connection = conx.Conxx();
            s = connection.createStatement();
            int z = s.executeUpdate("insert into agent_deli(codigo, cod_agente,cod_delin) values(" + AGDL.getCodigo() + "," + AGDL.getCodAgent() + "," + AGDL.getCodDelin() + ")");           
            if (z==1){
                JOptionPane.showMessageDialog(null,"El Agente ha sido asignado al Delincuente exitosamente","Alerta", JOptionPane.INFORMATION_MESSAGE);
            }else{
                JOptionPane.showMessageDialog(null,"Error al agregar, verifique datos","Alerta", JOptionPane.INFORMATION_MESSAGE);
            }
        }catch (Exception e){
            System.out.println("Error en el Query SQL: " + e);
        }
    }
    public ArrayList<String> loadAGNames(){
        ArrayList<String> loadAGNames = new ArrayList<>();
        try{
            connection = conx.Conxx();
            s = connection.createStatement();
            rs = s.executeQuery("select codigo, nombre from agent");           
            while(rs.next()){
                loadAGNames.add("Código: " + rs.getString("codigo") + " Nombre: " + rs.getString("nombre"));

            }
        }catch (Exception e){
            System.out.println("Error en el Query SQL: " + e);
        }
        return loadAGNames;
    }
    
    public ArrayList<objAGDL> loadDeli(int codigo){     
        try{
            connection = conx.Conxx();
            s = connection.createStatement();
            rs = s.executeQuery("SELECT ah.nombre AS nombre, d.nombre AS dnombre ,d.aliaa AS aliaz, tp.nombre AS tpnombre, o.nombre AS onombre FROM delincuentes d, agent ah, agent_deli aa, tipo_deli tp, organi o where d.codigo = aa.cod_delin AND ah.codigo = aa.cod_agente AND d.tipodel = tp.codigo AND d.organi = o.codigo AND ah.codigo = " + codigo + "order by d.nombre ASC");           
            while(rs.next()){
                objAGDL.listaDelin.add(new objAGDL(rs.getString("nombre"),rs.getString("dnombre"),rs.getString("aliaz"),rs.getString("tpnombre"),rs.getString("onombre")));
                
            }
        }catch (Exception e){
            System.out.println("Error en el Query SQL: " + e);
        }
        return objAGDL.listaDelin;
    }
    public ArrayList<String> loadcantTpdel(){   
        ArrayList<String> listatipdei = new ArrayList<>();
        try{
            connection = conx.Conxx();
            s = connection.createStatement();
            rs = s.executeQuery("select tp.nombre as nen from delincuentes d, tipo_deli tp where d.tipodel between 1 and 10 AND d.tipodel = tp.codigo AND d.tipodel between 1 and 10");           
            while(rs.next()){
                listatipdei.add(rs.getString("nen"));
                
            }
        }catch (Exception e){
            System.out.println("Error en el Query SQL: " + e);
        }
        return listatipdei;        
    }
}
